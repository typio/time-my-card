# Time My Card

## Usage
View [Time My Card](https://timemycard.com/) online!

## Future Features
- Save values in local storage ✅ v2.0.0
- Break deduction ✅ v2.5.0
- Pay calculation given payrate
- OT calculation ✅ v1.1.1
- 2/+ time cards in calculation ✅ v1.1.0
- Dark Mode ✅ v2.0.0
- Weekly and California OT calculation
- About page (routing?)

## About
I created this for personal use, and to try out ~~[Svelte](https://svelte.dev/)~~ now [Vue](https://vuejs.org/).